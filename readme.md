Development
-----------

1. Install `npm`;

2. Run `npm install`;

3. Run `node_modules/.bin/gulp build` to generate `dist`;

Installation
------------

1. Install dependencies

        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
        <script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/aes.js"></script>
        <script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/hmac-sha1.js"></script>
        <script src="//crypto-js.googlecode.com/svn/tags/3.1.2/build/components/enc-base64-min.js"></script>

2. Install the latest SDK(1.3)

        <script src="//vcard-sandbox.s3.amazonaws.com/js/sdk/vcard-sdk.latest.min.js"></script>

    or a specific SDK version

        <script src="//vcard-sandbox.s3.amazonaws.com/js/sdk/vcard-sdk.1.4.min.js"></script>


3. From here on you can either, _Connect with user access_

        Wallet = new MatchMove.WalletRequest(
            'some@one.com',
            'somesecurepassword',
            {
                // verbose: function () {return window;},
                server: 'https://beta-api.mmvpay.com',
                consumer: {
                    key: 'YourConsumerKey',
                    secret: 'YourConsumerSecret'}
        });

    a. Requesting for authentication. This is using the `$.ajax` of jquery which inherits all the events.

            Wallet.connect()
                .done(function (data) {
                    Wallet.getAccess()
                        .done(function (data) {
                            // wallet is ready!
                        })
                        
                        .fail(function(jqXHR, textStatus) {
                            // some error
                        });
                })
                
                .fail(function(jqXHR, textStatus) {
                    // authentication failed.
                });

4. or, _Anonymously_

        Wallet = new MatchMove.WalletRequest({
            // verbose: function () {return window;},
            server: 'https://beta-api.mmvpay.com',
            consumer: {
                key: 'YourConsumerKey',
                secret: 'YourConsumerSecret'}
        });

    * __vebose__

    Enable verbose by passing the object that has console.[[log](https://developer.mozilla.org/en-US/docs/Web/API/Console/log), [groupCollapsed](https://developer.mozilla.org/en-US/docs/Web/API/Console/groupCollapsed), [groupEnd](https://developer.mozilla.org/en-US/docs/Web/API/Console/groupEnd)] functions. Default is `false`.

    * __server__

    API domain to connect to. `https://beta-api.mmvpay.com` for __beta__ and `https://api.mmvpay.com` for __production__

    * __consumer.key__, __consumer.secret__

    Consumer credentials


5. To consume a resource,

        Wallet.consume(resource, method, params)
        .done(function (data) {
            // Success
        })
        
        .fail(function (xhr, status, err) {
            // Error
        })
        
        .complete(function () {
            // Always
        });